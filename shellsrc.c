#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/wait.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include<signal.h>

//This is a comment from the example branch

char *prompt;

void signal_handler(int no)
{
    printf("\n");
}

void run()
{
    char in[50];
    while(1)
    {
        printf("\n%s > ", prompt);
        fgets(in, 50, stdin);
        char *cmd = strtok(in, " \n");
        char *args[15];
        char *tmp = strtok(NULL, " \n");
        int i = 1;
        args[0] = cmd;
        do
        {
            args[i] = tmp;
            tmp = strtok(NULL, " \n");
        }while(tmp);
        if(cmd == NULL)continue;
        if(strcmp(cmd, "exit") == 0)
        {
            fputs("exiting the shell\n", stdout);
            return;
        }
        else if(strcmp(cmd, "pid") == 0)
        {
            printf("%d", getpid());
        }
        else if(strcmp(cmd, "ppid") == 0)
        {
            printf("%d", getppid());
        }
        else if(strncmp(cmd, "cd", 2) == 0)
        {
            if(args[1])
            {
                chdir(args[1]);
            }
            else
            {
                printf("%s", getcwd(NULL, 0));
            }
        }
        else
        {
            int i;
            int n = fork();
            if(n == 0)
            {
                i = execvp(args[0], args);
                exit(i);
            }
            else if(n > 0)
            {
                wait(&i);
                printf("\n %d", i);
            }
        }
    }
}

int main(int argc, char **argv)
{
    struct sigaction new_action;
    new_action.sa_handler = signal_handler;
    new_action.sa_flags = SA_NODEFER | SA_ONSTACK;
    sigaction(SIGINT, &new_action, NULL);
    if(argc > 2)
    {
        if(strcmp(argv[1], "-p") == 0)
        {
            prompt = argv[2];
        }
    }
    if(prompt == NULL)
    {
        prompt = "msh";
    }
    run();
    return 0;
}
