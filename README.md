# README #

This is a very basic shell currently under development as a personal project.

### My Shell (msh) ###

* This is a shell currently under development, started in CMSC 257 at VCU, under Dr. Ghosh
* 0.1.0

### How do I get set up? ###

* Download this repo.
* Create a ~/bin folder and add it to your path (Once I learn proper installation this may be deprecated)
* Run the command "make"
* You're all done!
* Does not require any additional dependencies
